(function () {
    "use strict";
    
    const todoList = document.querySelector('[data-todoList]');
    const todoForm = document.querySelector('[data-todoForm]');
    const todoFormInput = document.querySelector('[data-todoFormInput]');
    const createTodoButton = document.querySelector('[data-createTodoButton]');
    const templateTodoListItem = document.querySelector('[data-todoListItemTemplate]');
    let todoFormInputValue;
    let todosAppData = {
        currentId: 0,
        allTodos: {}
    };

    function checkInputContent() {
        todoFormInputValue = todoFormInput.value;

        if (todoFormInputValue.replace(/\s/g, '') === "") {
            createTodoButton.setAttribute('disabled', 'disabled');
        } else {
            createTodoButton.removeAttribute('disabled');
        }
    }

    function getTodos(callback) {
        const data = localStorage.getItem('todoAppData');

        if (data) {
            todosAppData = JSON.parse(data);
            callback(todosAppData.allTodos);
        }
    }

    function storeTodo(todoObject) {
        todosAppData.allTodos[todosAppData.currentId] = todoObject;
        todosAppData.currentId++;
        localStorage.setItem('todoAppData', JSON.stringify(todosAppData));
    }

    function updateTodos() {
        localStorage.setItem('todoAppData', JSON.stringify(todosAppData));
    }

    function sendTodo() {
        const todo = {
            id: todosAppData.currentId,
            task: todoFormInputValue,
            complete: false
        }

        storeTodo(todo);
        addTodo(todo);
    }

    function findParentListItem(todoId) {
        return todoList.querySelector(`[data-todoListItem="${todoId}"]`);
    }

    function deleteTodo() {
        const todoId = this.dataset.deletetodo;
        const parentListItem = findParentListItem(todoId);

        delete todosAppData.allTodos[todoId];

        if (Object.keys(todosAppData.allTodos).length === 0) {
            todosAppData.currentId = 0;
        }

        parentListItem.classList.add('hide');

        updateTodos();
    }

    function toggleComplete() {
        const todoId = this.dataset.togglecomplete;
        const parentListItem = findParentListItem(todoId);
        
        if (todosAppData.allTodos[todoId].complete) {
            todosAppData.allTodos[todoId].complete = false;

            parentListItem.classList.remove('complete');
        } else {
            todosAppData.allTodos[todoId].complete = true;

            parentListItem.classList.add('complete');
        }

        updateTodos();
    }

    function toggleEditTodo(selectedTodoId) {
        let todoId;
        
        if (typeof selectedTodoId === 'string') {
            todoId = selectedTodoId;
        } else {
            todoId = this.dataset.toggleedittodo;
        } 

        const parentListItem = findParentListItem(todoId);
        const editFormInput = parentListItem.querySelector('#editTodoInput');

        parentListItem.classList.toggle('edit-mode');
        editFormInput.value = todosAppData.allTodos[todoId].task;
        editFormInput.focus();
    }

    function editTodo(e) {
        e.preventDefault();

        const todoId = this.dataset.edittodoform;
        const parentListItem = findParentListItem(todoId);
        const editFormInput = parentListItem.querySelector('#editTodoInput');

        if (!(editFormInput.value.replace(/\s/g, '') === "") || !(editFormInput.value === todosAppData.allTodos[todoId].task)) {
            const newTaskValue = editFormInput.value;

            todosAppData.allTodos[todoId].task = newTaskValue;
            parentListItem.querySelector('[data-listItemTask]').innerText = newTaskValue;

            updateTodos();
        }

        toggleEditTodo(todoId);
    }

    function createTodoElement(data) {
        const cloneListItem = templateTodoListItem.content.cloneNode(true);
        const todoListItem = cloneListItem.querySelector('[data-todoListItem]');
        const listItemTask = cloneListItem.querySelector('[data-listItemTask]');
        const deleteButton = cloneListItem.querySelector('[data-deleteTodo]');
        const toggleCompleteButton = cloneListItem.querySelector('[data-toggleComplete]');
        const toggleEditButton = cloneListItem.querySelector('[data-toggleEditTodo]');
        const editTodoForm = cloneListItem.querySelector('[data-edittodoform]');

        if (data.complete) {
            todoListItem.classList.add('complete');
        }

        listItemTask.innerText = data.task;

        todoListItem.setAttribute('data-todoListItem', data.id);

        deleteButton.setAttribute('data-deleteTodo', data.id);
        deleteButton.addEventListener('click', deleteTodo);

        toggleCompleteButton.setAttribute('data-toggleComplete', data.id);
        toggleCompleteButton.addEventListener('click', toggleComplete);

        toggleEditButton.setAttribute('data-toggleEditTodo', data.id);
        toggleEditButton.addEventListener('click', toggleEditTodo);

        editTodoForm.setAttribute('data-edittodoform', data.id);
        editTodoForm.addEventListener('submit', editTodo);

        return cloneListItem;
    }

    function addTodo(todoObject) {
        const todo = createTodoElement(todoObject);
        todoList.prepend(todo);
    }

    function populateTodos() {
        const todoFragment = document.createDocumentFragment();

        todoList.innerHTML = '';
        
        Object.keys(todosAppData.allTodos).forEach(key => {
            const todoElement = createTodoElement(todosAppData.allTodos[key]);
            todoFragment.prepend(todoElement);
        });

        todoList.prepend(todoFragment);
    }

    function createTodo(e) {
        e.preventDefault();

        sendTodo();

        todoFormInput.value = '';

        checkInputContent();
        todoFormInput.focus();
    }

    todoForm.addEventListener('submit', createTodo);
    todoFormInput.addEventListener('input', checkInputContent);

    getTodos(populateTodos);
    todoFormInput.focus();
})();